EESchema Schematic File Version 2  date 29/10/2012 11:58:36
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Cachatuga-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 2 2
Title "Cachatuga"
Date "29 oct 2012"
Rev "0.1"
Comp "JMMP"
Comment1 "http://creativecommons.org/licenses/by-sa/3.0/"
Comment2 "Released under CC-BY-SA 3.0 Unported"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L JP_2 JP_OnOff1
U 1 1 508C7099
P 3550 3250
F 0 "JP_OnOff1" H 3610 3470 60  0000 C CNN
F 1 "JP_2" H 3590 3180 60  0001 C CNN
	1    3550 3250
	1    0    0    -1  
$EndComp
$Comp
L HC06 U1
U 1 1 5085B8ED
P 5750 3100
F 0 "U1" H 5750 3100 60  0000 C CNN
F 1 "HC06" H 5750 3100 60  0000 C CNN
	1    5750 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2250 4550 2250
Wire Wire Line
	6900 2150 7300 2150
Wire Wire Line
	3550 3250 3150 3250
Wire Wire Line
	6400 2150 6800 2150
Connection ~ 4350 3750
Wire Wire Line
	4350 3950 4350 3750
Wire Wire Line
	4550 3350 4150 3350
Wire Wire Line
	4550 2150 4100 2150
Wire Wire Line
	6800 4250 6800 4450
Wire Wire Line
	5150 3750 4150 3750
Wire Wire Line
	4150 3750 4150 3350
Wire Wire Line
	6400 3150 6800 3150
Wire Wire Line
	6800 3150 6800 3350
Wire Wire Line
	3650 3250 4550 3250
$Comp
L JP_2 JP_AT1
U 1 1 5085A4FF
P 6800 2150
F 0 "JP_AT1" H 6860 2370 60  0000 C CNN
F 1 "JP_2" H 6840 2080 60  0001 C CNN
	1    6800 2150
	1    0    0    -1  
$EndComp
Text HLabel 7300 2150 2    60   Input ~ 0
3V3
Text HLabel 3150 3250 0    60   Input ~ 0
3V3
Text HLabel 4100 2250 0    60   Input ~ 0
RX
Text HLabel 4100 2150 0    60   Input ~ 0
TX
Text HLabel 4150 3350 0    60   Input ~ 0
GND
Text Notes 7000 3450 3    60   ~ 0
Red\nStatus
$Comp
L GND #PWR12
U 1 1 4E33C5F2
P 6800 4450
F 0 "#PWR12" H 6800 4450 30  0001 C CNN
F 1 "GND" H 6800 4380 30  0001 C CNN
	1    6800 4450
	1    0    0    -1  
$EndComp
$Comp
L LED D202
U 1 1 4E33C5C6
P 6800 4050
F 0 "D202" H 6800 4150 50  0000 C CNN
F 1 "LED" H 6800 3950 50  0000 C CNN
	1    6800 4050
	0    1    1    0   
$EndComp
$Comp
L R R203
U 1 1 4E33C5BE
P 6800 3600
F 0 "R203" V 6880 3600 50  0000 C CNN
F 1 "330" V 6800 3600 50  0000 C CNN
	1    6800 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR11
U 1 1 4E33C4B0
P 4350 3950
F 0 "#PWR11" H 4350 3950 30  0001 C CNN
F 1 "GND" H 4350 3880 30  0001 C CNN
	1    4350 3950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
