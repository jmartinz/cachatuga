EESchema Schematic File Version 2  date 29/10/2012 11:58:36
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Cachatuga-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 2
Title "LibreTooth"
Date "29 oct 2012"
Rev "0.1"
Comp "Fabio Varesano - varesano.net"
Comment1 "http://creativecommons.org/licenses/by-sa/3.0/"
Comment2 "Released under CC-BY-SA 3.0 Unported"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1200 1600 900  1600
Wire Wire Line
	900  1600 900  4250
Wire Wire Line
	3700 2000 4150 2000
Wire Wire Line
	2100 4050 1300 4050
Wire Wire Line
	1200 4050 900  4050
Wire Wire Line
	6800 1250 6800 1200
Wire Wire Line
	6800 1200 6050 1200
Wire Wire Line
	6050 2400 6350 2400
Wire Wire Line
	2100 3600 1400 3600
Wire Wire Line
	1400 3600 1400 3450
Connection ~ 1850 3100
Wire Wire Line
	1300 3450 1300 3700
Wire Wire Line
	1300 3700 1850 3700
Wire Wire Line
	1850 3700 1850 1000
Connection ~ 900  3450
Wire Wire Line
	1200 3450 900  3450
Wire Wire Line
	6050 2700 6350 2700
Wire Wire Line
	1400 2900 2100 2900
Wire Wire Line
	6300 1800 6050 1800
Wire Wire Line
	6050 2000 6300 2000
Wire Wire Line
	1400 1600 2100 1600
Wire Wire Line
	1850 1700 1300 1700
Wire Wire Line
	1850 2150 1300 2150
Wire Wire Line
	1300 2150 1300 2050
Connection ~ 1850 2550
Wire Wire Line
	1850 2550 1300 2550
Wire Wire Line
	1300 2550 1300 2450
Connection ~ 1850 1700
Wire Wire Line
	1300 1700 1300 1600
Connection ~ 900  2900
Wire Wire Line
	900  2900 1200 2900
Connection ~ 900  2050
Wire Wire Line
	4150 1700 3200 1700
Wire Wire Line
	3200 1700 3200 1350
Wire Wire Line
	4150 1800 3600 1800
Wire Wire Line
	4600 5000 5000 5000
Wire Wire Line
	4450 4250 4450 3850
Wire Wire Line
	4450 3850 3950 3850
Wire Wire Line
	3550 4550 4250 4550
Wire Wire Line
	7350 2250 7350 2950
Wire Wire Line
	7350 2950 7800 2950
Wire Wire Line
	7800 3300 7350 3300
Wire Wire Line
	7350 3300 7350 3750
Wire Wire Line
	3950 4350 3950 4550
Connection ~ 3950 4550
Wire Wire Line
	4950 4400 4950 4550
Wire Wire Line
	4650 4550 5200 4550
Connection ~ 4950 4550
Wire Wire Line
	4100 5000 3550 5000
Wire Wire Line
	3600 1800 3600 2750
Wire Wire Line
	4150 1600 3750 1600
Wire Wire Line
	3750 1600 3750 1350
Wire Wire Line
	900  2450 1200 2450
Connection ~ 900  2450
Connection ~ 1850 2150
Wire Wire Line
	1300 2900 1300 3100
Wire Wire Line
	1300 3100 1850 3100
Wire Wire Line
	6300 2300 6050 2300
Wire Wire Line
	1400 2050 2100 2050
Wire Wire Line
	6050 1400 6300 1400
Wire Wire Line
	2100 2450 1400 2450
Wire Wire Line
	6350 2600 6050 2600
Wire Wire Line
	1500 3450 2100 3450
Wire Wire Line
	6050 2500 6350 2500
Wire Wire Line
	4150 1900 3600 1900
Connection ~ 3600 1900
Connection ~ 900  4050
Wire Wire Line
	900  2050 1200 2050
Text Label 3700 2000 0    60   ~ 0
VIn
Text Label 2100 4050 0    60   ~ 0
VIn
$Comp
L JP_2 VIn/GND1
U 1 1 508B2C42
P 1200 4050
F 0 "VIn/GND1" H 1260 4270 60  0000 C CNN
F 1 "JP_2" H 1240 3980 60  0001 C CNN
	1    1200 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR8
U 1 1 508A6B12
P 6800 1250
F 0 "#PWR8" H 6800 1250 30  0001 C CNN
F 1 "GND" H 6800 1180 30  0001 C CNN
	1    6800 1250
	1    0    0    -1  
$EndComp
Text Label 6350 2400 0    60   ~ 0
OM SDIO
Text Label 6350 2500 0    60   ~ 0
OM SCLK
Text Label 2100 3600 0    60   ~ 0
OM SDIO
Text Label 2100 3450 0    60   ~ 0
OM SCLK
$Comp
L JP_4 OptMouse1
U 1 1 508A6210
P 1200 3450
F 0 "OptMouse1" H 1360 3670 60  0000 C CNN
F 1 "JP_4" H 1350 3390 60  0001 C CNN
	1    1200 3450
	1    0    0    -1  
$EndComp
$Comp
L OSHW-LOGO #G1
U 1 1 5085CD19
P 10500 7000
F 0 "#G1" H 10500 6697 60  0001 C CNN
F 1 "OSHW-LOGO" H 10500 7303 60  0001 C CNN
	1    10500 7000
	1    0    0    -1  
$EndComp
Text Label 6350 2600 0    60   ~ 0
RX_5V
Text Label 6350 2700 0    60   ~ 0
TX_5V
Text Label 6300 1400 0    60   ~ 0
SLibre
Text Label 6300 1800 0    60   ~ 0
SLapiz
Text Label 2100 2900 0    60   ~ 0
SLibre
Text Label 2100 2450 0    60   ~ 0
SLapiz
Text Label 6300 2000 0    60   ~ 0
SCI
Text Label 2100 2050 0    60   ~ 0
SCI
Text Label 6300 2300 0    60   ~ 0
SCD
Text Label 2100 1600 0    60   ~ 0
SCD
$Comp
L +5V #PWR2
U 1 1 50853CB6
P 1850 1000
F 0 "#PWR2" H 1850 1090 20  0001 C CNN
F 1 "+5V" H 1850 1090 30  0000 C CNN
	1    1850 1000
	1    0    0    -1  
$EndComp
Text Label 1850 1200 0    60   ~ 0
5V
Text Label 900  4150 0    60   ~ 0
GND
$Comp
L GND #PWR1
U 1 1 50853C16
P 900 4250
F 0 "#PWR1" H 900 4250 30  0001 C CNN
F 1 "GND" H 900 4180 30  0001 C CNN
	1    900  4250
	1    0    0    -1  
$EndComp
$Comp
L JP_3 Servo4
U 1 1 50853BBC
P 1200 2900
F 0 "Servo4" H 1300 3120 60  0000 C CNN
F 1 "JP_3" H 1280 2830 60  0001 C CNN
	1    1200 2900
	1    0    0    -1  
$EndComp
$Comp
L JP_3 Servo2
U 1 1 50853BB6
P 1200 2050
F 0 "Servo2" H 1300 2270 60  0000 C CNN
F 1 "JP_3" H 1280 1980 60  0001 C CNN
	1    1200 2050
	1    0    0    -1  
$EndComp
$Comp
L JP_3 Servo3
U 1 1 50853BB0
P 1200 2450
F 0 "Servo3" H 1300 2670 60  0000 C CNN
F 1 "JP_3" H 1280 2380 60  0001 C CNN
	1    1200 2450
	1    0    0    -1  
$EndComp
$Comp
L JP_3 Servo1
U 1 1 5085396A
P 1200 1600
F 0 "Servo1" H 1300 1820 60  0000 C CNN
F 1 "JP_3" H 1280 1530 60  0001 C CNN
	1    1200 1600
	1    0    0    -1  
$EndComp
$Sheet
S 7800 2350 1900 1500
U 4E33CBF2
F0 "HC05 Bluetooth" 60
F1 "HC05_BT.sch" 60
F2 "GND" I L 7800 3300 60 
F3 "3V3" I L 7800 2950 60 
F4 "RESET" I R 9700 2600 60 
F5 "RX" I R 9700 2850 60 
F6 "TX" I R 9700 3100 60 
$EndSheet
$Comp
L ARDUINO_SHIELD Cachatuga1
U 1 1 507E98DB
P 5100 1900
F 0 "Cachatuga1" H 4750 2850 60  0000 C CNN
F 1 "ARDUINO_SHIELD" H 5150 950 60  0000 C CNN
	1    5100 1900
	1    0    0    -1  
$EndComp
Text Notes 800  6750 0    60   ~ 0
KiCAD specific stuff. \nJust for ERC validation.
Text Label 1000 6550 0    60   ~ 0
GND
Text Label 1400 6550 0    60   ~ 0
5V
$Comp
L PWR_FLAG #FLG2
U 1 1 4E33DB31
P 1400 6550
F 0 "#FLG2" H 1400 6820 30  0001 C CNN
F 1 "PWR_FLAG" H 1400 6780 30  0000 C CNN
	1    1400 6550
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG1
U 1 1 4E33DB26
P 1000 6550
F 0 "#FLG1" H 1000 6820 30  0001 C CNN
F 1 "PWR_FLAG" H 1000 6780 30  0000 C CNN
	1    1000 6550
	1    0    0    -1  
$EndComp
Text Label 3600 2400 0    60   ~ 0
GND
Text Label 3200 1550 0    60   ~ 0
5V
Text Label 9700 3100 0    60   ~ 0
TX
Text Label 9700 2850 0    60   ~ 0
RX
Text Label 9700 2600 0    60   ~ 0
RESET
Text Label 5000 5000 0    60   ~ 0
RX_5V
Text Label 3550 5000 2    60   ~ 0
RX
Text Label 3550 4550 2    60   ~ 0
TX
Text Label 5200 4550 0    60   ~ 0
TX_5V
$Comp
L R R103
U 1 1 4E33D1FA
P 4350 5000
F 0 "R103" V 4430 5000 50  0000 C CNN
F 1 "10K" V 4350 5000 50  0000 C CNN
	1    4350 5000
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR7
U 1 1 4E33D1D0
P 4950 3900
F 0 "#PWR7" H 4950 3990 20  0001 C CNN
F 1 "+5V" H 4950 3990 30  0000 C CNN
	1    4950 3900
	1    0    0    -1  
$EndComp
$Comp
L R R106
U 1 1 4E33D189
P 4950 4150
F 0 "R106" V 5030 4150 50  0000 C CNN
F 1 "10K" V 4950 4150 50  0000 C CNN
	1    4950 4150
	1    0    0    -1  
$EndComp
$Comp
L 3V3 #PWR6
U 1 1 4E33D179
P 3950 3850
F 0 "#PWR6" H 3950 3950 40  0001 C CNN
F 1 "3V3" H 3950 3975 40  0000 C CNN
	1    3950 3850
	1    0    0    -1  
$EndComp
$Comp
L R R101
U 1 1 4E33D140
P 3950 4100
F 0 "R101" V 4030 4100 50  0000 C CNN
F 1 "10K" V 3950 4100 50  0000 C CNN
	1    3950 4100
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q101
U 1 1 4E33D0DF
P 4450 4450
F 0 "Q101" H 4460 4620 60  0000 R CNN
F 1 "MOSFET_N" H 4460 4300 60  0000 R CNN
	1    4450 4450
	0    1    1    0   
$EndComp
$Comp
L GND #PWR10
U 1 1 4E33CDB8
P 7350 3750
F 0 "#PWR10" H 7350 3750 30  0001 C CNN
F 1 "GND" H 7350 3680 30  0001 C CNN
	1    7350 3750
	1    0    0    -1  
$EndComp
$Comp
L 3V3 #PWR9
U 1 1 4E33CDB1
P 7350 2250
F 0 "#PWR9" H 7350 2350 40  0001 C CNN
F 1 "3V3" H 7350 2375 40  0000 C CNN
	1    7350 2250
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR3
U 1 1 4E33C2DC
P 3200 1350
F 0 "#PWR3" H 3200 1440 20  0001 C CNN
F 1 "+5V" H 3200 1440 30  0000 C CNN
	1    3200 1350
	1    0    0    -1  
$EndComp
$Comp
L 3V3 #PWR5
U 1 1 4E33C28E
P 3750 1350
F 0 "#PWR5" H 3750 1450 40  0001 C CNN
F 1 "3V3" H 3750 1475 40  0000 C CNN
	1    3750 1350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR4
U 1 1 4E33C280
P 3600 2750
F 0 "#PWR4" H 3600 2750 30  0001 C CNN
F 1 "GND" H 3600 2680 30  0001 C CNN
	1    3600 2750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
